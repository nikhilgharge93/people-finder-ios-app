//
//  main.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
