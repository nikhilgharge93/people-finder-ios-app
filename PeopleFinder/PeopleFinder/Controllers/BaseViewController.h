


@interface BaseViewController : UIViewController

@property (nonatomic, assign) CGFloat animatedDistance;
- (void) animateBlock: (void (^)(void)) block;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
- (void)textFieldDidBeginEditing:(UITextField *)textField;
- (void)textFieldDidEndEditing:(UITextField *)textField;
@end
