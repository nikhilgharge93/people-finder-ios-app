//
//  SearchViewController.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customCell.h"
#import "Employee.h"
#import "BaseViewController.h"
@protocol SecondDelegate <NSObject>
-(void) secondViewControllerDismissed:(Employee *)newAdmin;
@end
@interface SearchViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate,UISearchControllerDelegate>
@property(strong,nonatomic)Employee *currentLoggedInUser;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *gestureRecognizer;
@property(strong, nonatomic) NSMutableArray* arrEmployees;
@property (weak, nonatomic) IBOutlet UITableView *searchResultsTblvw;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UISearchBar *theSearchBar;
@property (weak, nonatomic) IBOutlet UILabel *displayLabelForListOfManagers;
@property(strong, nonatomic) UIButton* button;
@property (strong, nonatomic) NSIndexPath* index;
@property (assign, nonatomic) BOOL isAdminSearchVc;
@property(strong, nonatomic)Employee* updateAdminDetails;
@property(strong, nonatomic)NSIndexPath* indexOfSelectedAdmin;
@property (nonatomic, assign) id<SecondDelegate>    myDelegate;
- (IBAction)backToLoginPage:(UIBarButtonItem *)sender;



@end
