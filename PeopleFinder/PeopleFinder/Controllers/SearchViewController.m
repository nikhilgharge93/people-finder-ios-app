//
//  SearchViewController.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "SearchViewController.h"
#import "PerformSearchOperations.h"
#import "DisplayInfoViewController.h"

@interface SearchViewController ()
@property (nonatomic, retain) UISearchController *searchController;
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton = YES;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    if (self.isAdminSearchVc){
        self.title = kAdminSearch;
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController:)];
        [self.navigationItem setLeftBarButtonItem:leftBarButton];
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(saveSelectedAdmin:)];
        [self.navigationItem setRightBarButtonItem:rightBarButton];
        self.displayLabelForListOfManagers.text =  NSLocalizedString(@"Search to select an Admin",nil);
    }
    else {
        self.button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 65, 44)];
        [self.button setTitle:[NSString stringWithFormat:@"%@ %@",self.currentLoggedInUser.firstName,self.currentLoggedInUser.lastName] forState:UIControlStateNormal];
        [self.button addTarget:self action:@selector(showProfile) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.titleView = self.button;
        self.displayLabelForListOfManagers.text =  NSLocalizedString(@"Search to select the manager",nil);
    }
        
    //    UIButton* customButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [customButton setImage:[UIImage imageNamed:@"logInImage"] forState:UIControlStateNormal];
    //    UIBarButtonItem* customBarButton = [[UIBarButtonItem alloc]initWithCustomView:customButton];
    //    self.navigationItem.leftBarButtonItem = customBarButton;
    //     [self.searchResultsTblvw addGestureRecognizer:self.gestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrEmployees.count;
}

-(CustomCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    Employee* empObj = [self.arrEmployees objectAtIndex:indexPath.row];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@, %@",empObj.firstName,empObj.lastName];
    cell.phoneLabel.text = [empObj.phoneNumber stringValue];
    if (self.isAdminSearchVc){
        if(cell.accessoryType == UITableViewCellAccessoryNone) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.index = indexPath;
    NSLog(@"Index %@",self.index);
    if (self.isAdminSearchVc){
        UITableViewCell *cellTapped = [tableView cellForRowAtIndexPath:indexPath];
        if(cellTapped.accessoryType == UITableViewCellAccessoryNone) {
            cellTapped.accessoryType = UITableViewCellAccessoryCheckmark;
            self.indexOfSelectedAdmin = indexPath;
        }
        else {
            cellTapped.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else{
        [self performSegueWithIdentifier:@"showDetailsViewController" sender:self];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
     if (self.isAdminSearchVc){
         [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
         self.indexOfSelectedAdmin = indexPath;
     }
}



- (IBAction)backToLoginPage:(UIBarButtonItem *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.theSearchBar resignFirstResponder];
    [self.theSearchBar setShowsCancelButton:NO animated: YES];
}

-(void)showProfile {
    [self performSegueWithIdentifier:@"showDetailViewController" sender:self.button];
    
}


-(void) getEmployeesForSearchString: (NSString*) strSearch{
    
    if ([Utilities isConnected]) {
        [Utilities showBusyIndicator:NSLocalizedString(@"Loading",nil) withTitle:@"" toViewController:self dimBackground:NO];
        PerformSearchOperations* performSearchOperation = [[PerformSearchOperations alloc] initOperationWithSearchString:strSearch];
        __weak __typeof(self)weakSelf = self;
        [performSearchOperation performSearchOpn:^(NSMutableArray* allEmployees) {
            
            weakSelf.arrEmployees = [NSMutableArray arrayWithCapacity:[allEmployees count]];
            [weakSelf.arrEmployees addObjectsFromArray:allEmployees];
            [self.searchResultsTblvw reloadData];
            NSLog(@"array:%@",weakSelf.arrEmployees);
            [weakSelf.searchResultsTblvw reloadData];
            weakSelf.displayLabelForListOfManagers.text = [NSString stringWithFormat:@"%lu Results Found", (unsigned long)weakSelf.arrEmployees.count];
            [Utilities hideHUD];
        } andFailure:^(NSError *error) {
            [Utilities hideHUD];
            [Utilities showHUDWithTextOnly:NSLocalizedString(@"Request Failed",) onView:weakSelf.view];
        }];
    }
    else{
        [Utilities showHUDWithTextOnly:NSLocalizedString(@"Not Connected To the internet",nil) onView:self.view];
    }
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated: YES];
    searchBar.text = @"";
    if (self.isAdminSearchVc)
        self.displayLabelForListOfManagers.text =  NSLocalizedString(@"Search to select an Admin",nil);
    else
        self.displayLabelForListOfManagers.text =  NSLocalizedString(@"Search to select the manager",nil);
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self.arrEmployees removeAllObjects];
    if (self.isAdminSearchVc)
        self.displayLabelForListOfManagers.text =  NSLocalizedString(@"Search to select an Admin",nil);
    else
        self.displayLabelForListOfManagers.text =  NSLocalizedString(@"Search to select the manager",nil);
    [self.searchResultsTblvw reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:YES animated: YES];
    NSRange rangeOfString = [searchBar.text.lowercaseString rangeOfString:@"john"];
    if (rangeOfString.location != NSNotFound) {
        [self getEmployeesForSearchString:searchBar.text];
        
    }else{
        [self.searchResultsTblvw reloadData];
        self.displayLabelForListOfManagers.text = NSLocalizedString(@"0 Results Found",nil);
        
    }
    
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DisplayInfoViewController* displayViewObject = [segue destinationViewController];
    if ([[segue identifier] isEqualToString:@"showDetailsViewController"]) {
        displayViewObject.navigationItem.rightBarButtonItem = nil;
        displayViewObject.empObj = self.arrEmployees[self.index.row];
        displayViewObject.mailValue = YES;
        displayViewObject.callValue = YES;
    }
    else if ([[segue identifier]isEqualToString:@"showDetailViewController"]){
        displayViewObject.empObj = self.currentLoggedInUser;
        
    }
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated: YES];
}

-(void)dismissViewController: (UIButton *) sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveSelectedAdmin: (UIButton *) sender{
    self.updateAdminDetails = self.arrEmployees[self.indexOfSelectedAdmin.row];
    if([self.myDelegate respondsToSelector:@selector(secondViewControllerDismissed:)])
    {
        [self.myDelegate secondViewControllerDismissed:self.updateAdminDetails];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
 #pragma mark - Navigation
 
 
 - (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
 [searchBar resignFirstResponder];
 [searchBar setShowsCancelButton:YES animated: YES];
 if ([searchBar.text isEqualToString:@"john"]) {
 [self displayDataOnScreen];
 }
 }*/

@end

