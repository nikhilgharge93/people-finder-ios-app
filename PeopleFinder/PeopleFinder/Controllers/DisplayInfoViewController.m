//
//  DisplayInfoViewController.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "DisplayInfoViewController.h"
#import "ChangeableCustomCell.h"
#import "PerformDetailOperations.h"
#import "Employee.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SearchViewController.h"

#define kTextFieldSelectedColor [UIColor colorWithRed:23.0f/255.0f green:129.0f/255.0f blue:237.0f/255.0f alpha:1.0]
#define kTextFieldUnSelectedColor [UIColor grayColor]

@interface DisplayInfoViewController ()
@property(strong,nonatomic) Employee* updateEmpDetails;
@end

@implementation DisplayInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.editLabel = false;
    self.arrayOfPersonDataFields = [[NSMutableArray alloc]initWithObjects:@"First Name",@"Last Name",@"Mail",@"Mobile",@"City",@"Admin Name",@"Role",@"Direct Reports",@"Scope", nil];
    self.updateEmpDetails = [[Employee alloc] init];
    [self getEmployeesDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)searchButtonPressed:(UIButton *)sender {
    SearchViewController* searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    searchVC.myDelegate = self;
    searchVC.isAdminSearchVc = YES;
    UINavigationController* navgationBar = [[UINavigationController alloc]initWithRootViewController:searchVC];
    [self presentViewController:navgationBar animated:YES completion:nil];
        
    
}

- (IBAction)phoneCellTapped:(UIButton *)sender {
    
    if (self.callValue == YES) {
        NSString* phoneStr = [[NSString alloc]initWithFormat:@"tel:%@",self.empObj.phoneNumber];
        NSURL* phoneUrl = [[NSURL alloc]initWithString:phoneStr];
        [[UIApplication sharedApplication]openURL:phoneUrl];
    }
    
}

- (IBAction)phoneCellForWorkTapped:(UIButton *)sender {
    
    if (self.callValue == YES) {
        NSString* phoneStr = [[NSString alloc]initWithFormat:@"tel:%@",self.empObj.phoneNumberForWork];
        NSURL* phoneUrl = [[NSURL alloc]initWithString:phoneStr];
        [[UIApplication sharedApplication]openURL:phoneUrl];
    }}




-(void)getEmployeesDetails{
    if ([Utilities isConnected]) {
        [Utilities showBusyIndicator:NSLocalizedString(@"Loading",nil) withTitle:@"" toViewController:self dimBackground:NO];
        PerformDetailOperations* performDetailOpn = [[PerformDetailOperations alloc]initOperationWithSearchString];
        __weak __typeof(self) weakSelf = self;
        [performDetailOpn performDetailOperation:^(Employee* employeeObj) {
            weakSelf.updateEmpDetails.firstName = self.empObj.firstName;
            weakSelf.updateEmpDetails.lastName = self.empObj.lastName;
            weakSelf.updateEmpDetails.emailId = employeeObj.emailId;
            weakSelf.updateEmpDetails.city = employeeObj.city;
            weakSelf.updateEmpDetails.admin = employeeObj.admin;
            weakSelf.updateEmpDetails.role = employeeObj.role;
            weakSelf.updateEmpDetails.phoneNumber = self.empObj.phoneNumber;
            weakSelf.updateEmpDetails.phoneNumberForWork = employeeObj.phoneNumberForWork;
            weakSelf.updateEmpDetails.scope = employeeObj.scope;
            weakSelf.updateEmpDetails.directReports = [employeeObj.directReports mutableCopy];
            [weakSelf.detailsTableView reloadData];
            [Utilities hideHUD];
        } andFailure:^(NSError *error) {
            NSLog(@"Error is :%@",error);
        }];
    }else{
        [Utilities showHUDWithTextOnly:NSLocalizedString(@"Not Connected To the internet",nil) onView:self.view];
    }


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* cellIdentifier = @"cellForChangingPersonalContents";
    if (indexPath.row == 0) {
        ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
        cell.textfieldValue.text = self.updateEmpDetails.firstName;
        cell.textfieldValue.userInteractionEnabled = NO;
        cell.LocationImage.image = [UIImage imageNamed:@"PersonImage"];
        cell.LocationImage.hidden = NO;
        cell.searchImage.hidden = YES;
        return cell;

    }else if (indexPath.row == 1){
        
        ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
        cell.textfieldValue.text = self.updateEmpDetails.lastName;
        cell.textfieldValue.userInteractionEnabled = NO;
        cell.LocationImage.hidden = YES;
        cell.searchImage.hidden = YES;
        return cell;

    }else if (indexPath.row == 2){
       
        ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
        cell.textfieldValue.text = self.updateEmpDetails.emailId;
        cell.textfieldValue.userInteractionEnabled = NO;
        cell.LocationImage.image = [UIImage imageNamed:@"Email"];
        cell.searchImage.hidden = YES;
        return cell;

    }else if (indexPath.row == 3){
        cellIdentifier = @"cellForPhoneNumbers";
        PhoneNumberCell*cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        NSLog(@"%@",self.empObj.phoneNumberForWork);

        //self.updateEmpDetails.phoneNumber = [NSNumber numberWithLong:[cell.phoneTextfieldForMobile.text integerValue]];
        //self.updateEmpDetails.phoneNumberForWork = [NSNumber numberWithLong:[cell.phoneTextFieldForWork.text integerValue]];
        cell.phoneTextfieldForMobile.text = [self.updateEmpDetails.phoneNumber stringValue];
        
        if ([Utilities isNilString:self.updateEmpDetails.phoneNumberForWork.stringValue]) {
            NSLog(@"No value Present");
        }else{
         cell.phoneTextFieldForWork.text = [self.updateEmpDetails.phoneNumberForWork stringValue];
        }
        [self setBottomBorderForTextField:cell.phoneTextfieldForMobile withColor:kTextFieldUnSelectedColor];
        [self setBottomBorderForTextField:cell.phoneTextFieldForWork withColor:kTextFieldUnSelectedColor];
        if (self.editButtonTapped == YES) {
            cell.phoneTextfieldForMobile.userInteractionEnabled = YES;
            cell.phoneTextFieldForWork.userInteractionEnabled = YES;
            [cell.phoneTextfieldForMobile becomeFirstResponder];
        }else{
            cell.phoneTextfieldForMobile.userInteractionEnabled = NO;
            cell.phoneTextFieldForWork.userInteractionEnabled = NO;
        }
        
        return cell;
    }
    else if (indexPath.row == 4){
   
    ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
    //self.updateEmpDetails.city = cell.textfieldValue.text;
    cell.textfieldValue.text = self.updateEmpDetails.city;
    [self setBottomBorderForTextField:cell.textfieldValue withColor:kTextFieldUnSelectedColor];
    if (self.editButtonTapped == YES ) {
            cell.textfieldValue.userInteractionEnabled = YES;
            cell.searchImage.hidden = YES;
    }else{
            cell.textfieldValue.userInteractionEnabled = NO;
    }
    return cell;
    }else if (indexPath.row == 5){
   
    ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
        cell.LocationImage.hidden = YES;
        cell.searchImage.hidden = YES;
      if (self.updateEmpDetails.admin.lastName != nil && self.updateEmpDetails.admin.lastName != nil) {
             cell.textfieldValue.text = [NSString stringWithFormat:@"%@ %@",self.updateEmpDetails.admin.firstName,self.updateEmpDetails.admin.lastName];
        }
        
        if (self.editButtonTapped == YES) {
            cell.textfieldValue.userInteractionEnabled = YES;
            [cell.searchImage setImage:[UIImage imageNamed:@"SearchImage"] forState:UIControlStateNormal];
            cell.searchImage.hidden = NO;
        }else{
            cell.textfieldValue.userInteractionEnabled = NO;
        }
        
    return cell;
       
    }
    else if (indexPath.row == 6){
   
    ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
        //self.updateEmpDetails.Role = cell.textfieldValue.text;
        cell.textfieldValue.text = self.updateEmpDetails.role;
        cell.LocationImage.hidden = YES;
        cell.textfieldValue.userInteractionEnabled = NO;
        [self setBottomBorderForTextField:cell.textfieldValue withColor:kTextFieldUnSelectedColor];
        if (self.editButtonTapped == YES ) {
            cell.textfieldValue.userInteractionEnabled = YES;
             cell.searchImage.hidden = YES;
            //self.editButtonTapped = NO;
        }
        else {
            
            cell.textfieldValue.userInteractionEnabled = NO;
        }
//        if (self.cancelButtonTapped == YES) {
//            cell.textfieldValue.userInteractionEnabled = NO;
//            self.cancelButtonTapped = NO;
//        }
            return cell;
    }else if (indexPath.row == 7){
        
        ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
//        cell.textfieldValue.text = [NSString stringWithFormat:@"%@ %@",self.updateEmpDetails.DirectReports.firstName,self.updateEmpDetails.DirectReports.lastName];
        cell.LocationImage.hidden = YES;
        cell.textfieldValue.userInteractionEnabled = NO;
        if (![Utilities isEmpty:self.updateEmpDetails.directReports]) {
            Employee *directReports = [self.updateEmpDetails.directReports firstObject];
            cell.textfieldValue.text = [NSString stringWithFormat:@"%@ %@",directReports.firstName, directReports.lastName];
        }
        
        if (self.editButtonTapped == YES ) {
            cell.textfieldValue.userInteractionEnabled = YES;
             cell.searchImage.hidden = YES;
            //self.editButtonTapped = NO;
        }
//        if (self.cancelButtonTapped == YES) {
//            cell.textfieldValue.userInteractionEnabled = NO;
//            self.cancelButtonTapped = NO;
//        }
        return cell;
    }else if (indexPath.row == 8){
        
        ChangeableCustomCell  *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.personInfoLabel.text = self.arrayOfPersonDataFields[indexPath.row];
        cell.textfieldValue.text = self.updateEmpDetails.scope;
        cell.LocationImage.hidden = YES;
        cell.textfieldValue.userInteractionEnabled = NO;
        if (self.editButtonTapped == YES ) {
            cell.textfieldValue.userInteractionEnabled = YES;
             cell.searchImage.hidden = YES;
            //self.editButtonTapped = NO;
        }
//        if (self.cancelButtonTapped == YES) {
//            cell.textfieldValue.userInteractionEnabled = NO;
//            self.cancelButtonTapped = NO;
//        }
        return cell;
    }


    return nil;
}

- (void) setBottomBorderForTextField:(UITextField *) textField withColor:(UIColor *) color{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = color.CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 3:
            return 128;
            break;
               default:
            break;
    }
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrayOfPersonDataFields count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        if (self.mailValue == YES) {
            
       if ([MFMailComposeViewController canSendMail]) {
            NSString* emailTitle = @"Test Email";
            NSString* messageBody = @"Test Subject!";
            NSArray* toRecipents = [NSArray arrayWithObject:@"support@gmail.com"];
            
            MFMailComposeViewController* mc = [[MFMailComposeViewController alloc]init];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            [mc setToRecipients:toRecipents];
            
            [self presentViewController:mc animated:YES completion:NULL];
        }else{
            UIAlertView* emailError = [[UIAlertView alloc] initWithTitle:@"Email Unavailable" message:@"Sorry, were unable to find an email account on your device.\nPlease setup an account in your devices settings and try again." delegate: self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [emailError show];
        }
        
     }
   }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)editProfile:(UIBarButtonItem *)sender {
    if (self.editButtonTapped) {
        [self dismissViewController];
    }
    else{
        self.editButtonTapped = YES;
        self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@"saveImage"];
        self.navigationItem.rightBarButtonItem.title = @"";
        self.navigationItem.hidesBackButton = YES;
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewController)];
        [self.navigationItem setLeftBarButtonItem:leftBarButton];
        [self.detailsTableView reloadData];
    }
    
}

-(void)dismissViewController{
     [self.navigationItem setLeftBarButtonItem:nil];
     self.navigationItem.hidesBackButton = NO;
    self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@""];
     self.navigationItem.rightBarButtonItem.title = @"Edit";
     self.editButtonTapped = NO;
    [self.detailsTableView reloadData];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [super textFieldDidBeginEditing:textField];
    [self setBottomBorderForTextField:textField withColor:kTextFieldSelectedColor];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [super textFieldDidEndEditing:textField];
    [self setBottomBorderForTextField:textField withColor:kTextFieldUnSelectedColor];
}

-(void)secondViewControllerDismissed:(Employee *)newAdmin{
    self.updateEmpDetails.Admin = newAdmin;
    NSLog(@"%@",self.updatedAdminDetails.firstName);
    NSLog(@"%@",self.updatedAdminDetails.lastName);
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:5 inSection:0];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [self.detailsTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];

}


@end
