//
//  DisplayInfoViewController.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneNumberCell.h"
#import "Employee.h"
#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SearchViewController.h"
@interface DisplayInfoViewController :BaseViewController <UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,SecondDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editLabel;
@property bool editButtonTapped;
@property(strong,nonatomic) NSMutableArray* arrayOfPersonInfo;
@property(strong,nonatomic)NSMutableArray* arrayOfPersonDataFields;
@property(strong, nonatomic) NSIndexPath *expandedIndexPath;
@property (weak, nonatomic) IBOutlet UIImageView *ProfileImage;
- (IBAction)editProfile:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UITableView *detailsTableView;
@property(strong,nonatomic)Employee* empObj;
@property bool cancelButtonTapped;
@property bool mailValue;
@property bool callValue;
@property (strong,nonatomic)Employee* updatedAdminDetails;



- (IBAction)searchButtonPressed:(UIButton *)sender;
- (IBAction)phoneCellTapped:(UIButton *)sender;
- (IBAction)phoneCellForWorkTapped:(UIButton *)sender;
-(void)getEmployeesDetails;
@end
