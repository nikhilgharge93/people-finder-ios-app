//
//  LoginViewController.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Employee.h"

@interface LoginViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *txtemail;
@property (weak, nonatomic) IBOutlet UITextField *txtpassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property(weak, nonatomic) Employee *currentUser;
- (IBAction)loginBtnTapped:(UIButton *)sender;
@end
