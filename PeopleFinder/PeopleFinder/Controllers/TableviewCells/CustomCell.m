//
//  customCell.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 18/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
