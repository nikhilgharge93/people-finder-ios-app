//
//  customCell.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 18/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end
