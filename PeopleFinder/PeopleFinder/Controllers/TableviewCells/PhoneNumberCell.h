//
//  PhoneNumberCell.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 18/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneNumberCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *phoneImg;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfieldForMobile;

@property (weak, nonatomic) IBOutlet UIImageView *PhoneImageForMobile;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageForWork;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextFieldForWork;

@end
