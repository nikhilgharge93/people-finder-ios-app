//
//  ChangeableCustomCell.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 19/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeableCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textfieldValue;
@property (weak, nonatomic) IBOutlet UILabel *personInfoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *LocationImage;
@property (weak, nonatomic) IBOutlet UIButton *searchImage;

@end
