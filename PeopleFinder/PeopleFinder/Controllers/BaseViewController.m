
#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
        CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
        CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
        CGFloat numerator = midline - viewRect.origin.y - kMinimumScrollFraction * viewRect.size.height;
        CGFloat denominator = (kMaximumScrollFraction - kMinimumScrollFraction) * viewRect.size.height;
        CGFloat heightFraction = numerator / denominator;
        if (heightFraction < 0.0){
            heightFraction = 0.0;
        }
        else if (heightFraction > 1.0){
            heightFraction = 1.0;
        }
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){
            self.animatedDistance = floor(kKeyboardHeightPortraitMode * heightFraction);
        }
        else{
            self.animatedDistance = floor(kKeyboardHeightPortraitMode * heightFraction);
        }
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y -= self.animatedDistance;
        __weak __typeof(self)weakSelf = self;
        [self animateBlock:^{
            [weakSelf.view setFrame:viewFrame];
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y += self.animatedDistance;
        __weak __typeof(self)weakSelf = self;
        [self animateBlock:^{
            [weakSelf.view setFrame:viewFrame];
        }];
        self.animatedDistance = 0.0f;
    }
}

- (void) animateBlock: (void (^)(void)) block {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:kDefaultAnimationDuration];
    block();
    [UIView commitAnimations];
}

#pragma mark - View Methods

- (IBAction)viewBackgroundTapped:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}
@end
