//
//  LoginViewController.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 16/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "LoginViewController.h"
#import "PerformLoginOperation.h"
#import "Employee.h"
#import "SearchViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginBtnTapped:(UIButton *)sender {
    if ([Utilities isNilString:self.txtemail.text] || [Utilities isNilString:self.txtpassword.text]) {
        DLog(@"Error: Email or password missing");
        [Utilities showHUDWithTextOnly:NSLocalizedString(@"Error: Email or password missing", nil) onView:self.view];
    }
    else{
        [self.view endEditing:YES];
        
        if ([self.txtemail.text isEqualToString:@"demo"] && [self.txtpassword.text isEqualToString:@"demo"]) {
            if ([Utilities isConnected]) {
                [Utilities showBusyIndicator:NSLocalizedString(@"Loading", nil) withTitle:@"" toViewController:self dimBackground:NO];
                
                PerformLoginOperation *performAuthenticationOperation = [[PerformLoginOperation alloc] initOperation];
                __weak __typeof(self)weakSelf = self;
                [performAuthenticationOperation performLoginAuthentication: ^(Employee* currentLoggedInUser)
                 {
                     //                NSArray * emp = [allemployees filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"userName == %@ && password == %@",self.txtemail.text, self.txtpassword.text]];
                     
                     [Utilities hideHUD];
                     if (currentLoggedInUser != nil)
                     {      self.currentUser = currentLoggedInUser;
                         [weakSelf performSegueWithIdentifier:@"loginBtnIdentifier" sender:nil];
                     }
                     else {
                         [Utilities showHUDWithTextOnly:NSLocalizedString(@"Please enter valid Credentials", nil) onView:weakSelf.view];
                     }
                 } andFailure:^(NSError *error)
                 {
                     [Utilities hideHUD];
                     [Utilities showHUDWithTextOnly:NSLocalizedString(@"Request Failed", nil) onView:weakSelf.view];
                 }];
            }
            else{
                [Utilities showHUDWithTextOnly:NSLocalizedString(@"Not connected to Internet", nil) onView:self.view];
            }
 
        }else{
             
    }
        
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"loginBtnIdentifier"]) {
       SearchViewController* svc = [segue destinationViewController];
        svc.currentLoggedInUser = self.currentUser;
        svc.isAdminSearchVc = NO;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [super textFieldShouldReturn:textField];
    if (textField == self.txtpassword) {
        [self loginBtnTapped:self.btnSignIn];
    }
    return YES;
}

@end
