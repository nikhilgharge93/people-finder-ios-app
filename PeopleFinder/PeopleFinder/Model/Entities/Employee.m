//
//  Employee.m
//  PeopleFinder
//
//  Created by Harshad Khedekar on 19/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "Employee.h"

@implementation Employee
+ (Employee *)instanceFromDictionary:(NSDictionary *)aDictionary {
    Employee *instance = [[Employee alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
//    [instance setAttributesFromDirectReport:aDictionary];
    return instance;
}
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    self.userName = [Utilities checkForEmpty:(NSString *)aDictionary[@"userName"]];
    self.password = [Utilities checkForEmpty:(NSString *)aDictionary[@"password"]];
    self.image = [Utilities checkForEmpty:(NSString *)aDictionary[@"image"]];
    self.phoneNumber = [Utilities checkForEmpty:(NSNumber*)aDictionary[@"phoneNumber"]];
    self.phoneNumberForWork = [Utilities checkForEmpty:(NSNumber*)aDictionary[@"phoneNumberWork"]];
    self.firstName = [Utilities checkForEmpty:(NSString*)aDictionary[@"firstName"]];
    self.lastName = [Utilities checkForEmpty:(NSString*)aDictionary[@"lastName"]];
    self.empId = [Utilities checkForEmpty:(NSNumber*)aDictionary[@"empId"]];
    self.city = [Utilities checkForEmpty:(NSNumber*)aDictionary[@"city"]];
    self.emailId = [Utilities checkForEmpty:(NSString*)aDictionary[@"email"]];
    self.role = [Utilities checkForEmpty:(NSString*)aDictionary[@"role"]];
    self.scope = [Utilities checkForEmpty:(NSString*)aDictionary[@"scope"]];
    if ([aDictionary objectForKey:@"admin"]) {
        NSDictionary* adminDict = [Utilities checkForEmpty:(Employee*)aDictionary[@"admin"]];
        self.admin = [Employee setAttributesFromAdminDict:adminDict];
    }
    
    if ([aDictionary objectForKey:@"directreport"]) {
        NSMutableArray* directReportEmpDictArray = [Utilities checkForEmpty:(Employee*)aDictionary[@"directreport"]];
        self.directReports = [NSMutableArray arrayWithCapacity:[directReportEmpDictArray count]];
        self.directReports = [NSMutableArray arrayWithArray:[Employee setAttributesFromDirectReport:directReportEmpDictArray]];
   }
}


+(Employee *) setAttributesFromAdminDict:(NSDictionary *)aDictionary{
     Employee *admin = [[Employee alloc]init];
    admin.firstName = [Utilities checkForEmpty:(NSString*)aDictionary[@"firstName"]];
    admin.lastName = [Utilities checkForEmpty:(NSString*)aDictionary[@"lastName"]];
    return admin;
}

+(NSMutableArray*)setAttributesFromDirectReport:(NSArray *)arrEmp{
    
    NSMutableArray* directReportEmpArray = [[NSMutableArray alloc]init];
    directReportEmpArray = [NSMutableArray arrayWithCapacity:[arrEmp count]];
    for (NSDictionary* directReportDict in arrEmp) {
        Employee *directReportEmp = [[Employee alloc]init];
        directReportEmp.firstName = [Utilities checkForEmpty:(NSString*)directReportDict[@"firstName"]];
        directReportEmp.lastName = [Utilities checkForEmpty:(NSString*)directReportDict[@"lastName"]];
        [directReportEmpArray addObject:directReportEmp];
        
    }
    return directReportEmpArray;
}

#pragma mark - Encoding

- (void)encodeWithCoder:(NSCoder *)coder {
    ENCODE_OBJECT(userName);
    ENCODE_OBJECT(password);
    ENCODE_OBJECT(image);
    ENCODE_OBJECT(phoneNumber);
}

- (id)initWithCoder:(NSCoder *)coder {
    if((self = [super init])) // In a derived entity class: [super initWithCoder:coder]
    {
        DECODE_OBJECT(userName);
        DECODE_OBJECT(password);
        DECODE_OBJECT(image);
        DECODE_OBJECT(phoneNumber);
    }
    return self;
}

@end
