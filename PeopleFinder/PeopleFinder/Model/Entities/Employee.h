//
//  Employee.h
//  PeopleFinder
//
//  Created by Harshad Khedekar on 19/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "Entities.h"
@interface Employee : NSObject
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *image;
@property(strong,nonatomic) NSNumber* phoneNumber;
@property(strong,nonatomic) NSNumber* phoneNumberForWork;
@property(nonatomic,copy)NSString *firstName;
@property(nonatomic,copy)NSString *lastName;
@property(nonatomic,assign)NSNumber *empId;
@property(strong,nonatomic)NSString* emailId;
@property(strong,nonatomic)NSString* city;
@property(strong,nonatomic)Employee* admin;
//@property(strong,nonatomic)NSString* adminFirstName;
//@property(strong,nonatomic)NSString* adminLastName;
@property(strong,nonatomic)NSString* role;
@property(strong,nonatomic)NSMutableArray* directReports;
@property(strong,nonatomic)NSString* scope;



+ (Employee *)instanceFromDictionary:(NSDictionary *)aDictionary;
- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary;

@end
