// This is a good place for model enums, etc, common to more than one entity

#pragma mark - NSCoding Macros

#define ENCODE_OBJECT(aObject) [coder encodeObject:self.aObject forKey:@"" #aObject]
#define DECODE_OBJECT(aObject) self.aObject = [coder decodeObjectForKey:@"" #aObject]

#define ENCODE_BOOL(aBool) [coder encodeBool:self.aBool forKey:@"" #aBool]
#define DECODE_BOOL(aBool) self.aBool = [coder decodeBoolForKey:@"" #aBool]

#define ENCODE_INTEGER(aInteger) [coder encodeInteger:self.aInteger forKey:@"" #aInteger]
#define DECODE_INTEGER(aInteger) self.aInteger = [coder decodeIntegerForKey:@"" #aInteger]

#define ENCODE_FLOAT(aFloat) [coder encodeFloat:self.aFloat forKey:@"" #aFloat]
#define DECODE_FLOAT(aFloat) self.aFloat = [coder decodeFloatForKey:@"" #aFloat]

#define ENCODE_DOUBLE(aDouble) [coder encodeDouble:self.aDouble forKey:@"" #aDouble]
#define DECODE_DOUBLE(aDouble) self.aDouble = [coder decodeDoubleForKey:@"" #aDouble]

#pragma mark - Enums
typedef NS_ENUM(NSInteger, Passcode) {
    ChoosePasscode = 1,
    ConfirmPasscode,
    EnterPasscode
};

typedef NS_ENUM(NSInteger, ApproverType) {
    AmountApprover = 1,
    ExtraApprover
};