
#import "AppDelegate.h"
#import "OperationManager.h"
#import "AFHTTPRequestOperation.h"
#import "Operation.h"


//static NSString * const kAPIBaseURLString = @"http://knapp.sogeti.se/appitecture/";
@implementation OperationManager

+ (OperationManager *)sharedInstance {
    static OperationManager *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ _sharedInstance = [[OperationManager alloc] initWithBaseURL:[NSURL URLWithString:kApiUrl]]; });
    return _sharedInstance;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if(!self)
    {
        return nil;
    }
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    return self;
}

- (void)cancelAllOperations {
    [self.operationQueue cancelAllOperations];
}

- (void)cancelAllOperationsOfType:(Class)operationClass {
    for(AFHTTPRequestOperation *operation in self.operationQueue.operations)
    {
        if([operation isKindOfClass:operationClass])
        {
            [operation cancel];
        }
    }
}

- (void)cancelAllOperationsExceptOfType:(Class)operationClass {
    for(AFHTTPRequestOperation *operation in self.operationQueue.operations)
    {
        if(![operation isKindOfClass:operationClass])
        {
            [operation cancel];
        }
    }
}

@end
