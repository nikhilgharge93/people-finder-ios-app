
#import "AFHTTPRequestOperationManager.h"

@interface OperationManager : AFHTTPRequestOperationManager

+ (OperationManager *)sharedInstance;
- (void)cancelAllOperations;
- (void)cancelAllOperationsOfType:(Class)operationClass;
- (void)cancelAllOperationsExceptOfType:(Class)operationClass;

@end
