//
//  PerformLoginOperation.m
//  PeopleFinder
//
//  Created by Harshad Khedekar on 19/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "PerformLoginOperation.h"
#import "OperationManager.h"
#import "Employee.h"


//Final Login json
//http://www.mocky.io/v2/565ee6130f0000012d5772b2

@implementation PerformLoginOperation
- (id)initOperation {
    NSString *urlString = [[NSString stringWithFormat:@"%@/565ee6130f0000012d5772b2",kApiUrl]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];//
    NSMutableURLRequest *request =  [[OperationManager sharedInstance].requestSerializer requestWithMethod:@"GET" URLString:urlString parameters:nil];
    self = [self initWithRequest:request];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    //self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    self.securityPolicy = [OperationManager sharedInstance].securityPolicy;
    return self;
}

- (void)performLoginAuthentication:(CompletionHandler)successHandler andFailure:(ErrorHandler)errorHandler {
    [self setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"Response :: %@",responseObject);
        //NSDictionary *responseDict = (NSDictionary *)responseObject[@"Invoices"];
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        
       NSDictionary *LoggedInUser = responseDict[@"profile"];
        
       NSString* validMessage = responseObject[@"message"];
        if ([validMessage isEqualToString:@"success"]) {
            Employee *emp = [Employee instanceFromDictionary:LoggedInUser];
            successHandler(emp);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        ELog(error)
        errorHandler(error);
    }];
    [[OperationManager sharedInstance].operationQueue addOperation:self];
}
@end
