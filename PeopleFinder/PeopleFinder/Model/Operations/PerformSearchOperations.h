//
//  PerformSearchOperations.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 20/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "AFHTTPRequestOperation.h"
#import "Operation.h"

@interface PerformSearchOperations : AFHTTPRequestOperation
-(id)initOperationWithSearchString: (NSString*) strSearch;
-(void)performSearchOpn:(CompletionHandler)successHandler andFailure:(ErrorHandler)errorHandler;
@end
