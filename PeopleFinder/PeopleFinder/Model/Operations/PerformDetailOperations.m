//
//  PerformDetailOperations.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 30/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "PerformDetailOperations.h"
#import "OperationManager.h"
#import "Employee.h"


//_http://www.mocky.io/v2/5660316c120000162fabd868
@implementation PerformDetailOperations
-(id)initOperationWithSearchString{
    NSString* urlWithString = [[NSString stringWithFormat:@"%@/5660316c120000162fabd868",kApiUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest* request = [[OperationManager sharedInstance].requestSerializer requestWithMethod:@"GET" URLString:urlWithString parameters:nil];
    self = [self initWithRequest:request];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.securityPolicy = [OperationManager sharedInstance].securityPolicy;
    return self;
}

-(void)performDetailOperation:(CompletionHandler)successHandler andFailure:(ErrorHandler)errorHandler{
[self setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
    NSLog(@"ResponseObject:%@",responseObject);
    
    successHandler([Employee instanceFromDictionary:responseObject]);
} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    errorHandler(error);
}];
    [[OperationManager sharedInstance].operationQueue addOperation:self];
}
@end
