//
//  PerformLoginOperation.h
//  PeopleFinder
//
//  Created by Harshad Khedekar on 19/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "AFHTTPRequestOperation.h"
#import "Operation.h"

@interface PerformLoginOperation : AFHTTPRequestOperation

- (id)initOperation;
- (void)performLoginAuthentication:(CompletionHandler)successHandler andFailure:(ErrorHandler)errorHandler;

@end
