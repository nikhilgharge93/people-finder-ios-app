//
//  PerformSearchOperations.m
//  PeopleFinder
//
//  Created by Akash Malhotra on 20/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import "PerformSearchOperations.h"
#import "OperationManager.h"
#import "Employee.h"
//Initial json
//http://www.mocky.io/v2/564dcf250f00005a26d4e21f
//Final json
//http://www.mocky.io/v2/564f1dce1100009a252e3529

@implementation PerformSearchOperations
-(id)initOperationWithSearchString: (NSString*) strSearch {
    NSString* urlWithString = [[NSString stringWithFormat:@"%@/564f1dce1100009a252e3529",kApiUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest* request = [[OperationManager sharedInstance].requestSerializer requestWithMethod:@"GET" URLString:urlWithString parameters:nil];
    self = [self initWithRequest:request];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.securityPolicy = [OperationManager sharedInstance].securityPolicy;
    return self;
}
-(void)performSearchOpn:(CompletionHandler)successHandler andFailure:(ErrorHandler)errorHandler{
    [self setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"ResponseObject:%@",responseObject);
        NSDictionary* responseDict = responseObject;
        NSArray* employeesArray = responseDict[@"employees"];
        NSMutableArray* employees = [NSMutableArray arrayWithCapacity:[employeesArray count]];
        for (NSDictionary* item in employeesArray) {
            NSLog(@"Nsd:%@",item);
            [employees addObject:[Employee instanceFromDictionary:item]];}
            successHandler(employees);
      
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       errorHandler(error);
    }];
[[OperationManager sharedInstance].operationQueue addOperation:self];

}
@end
