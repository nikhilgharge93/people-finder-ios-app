//
//  PerformDetailOperations.h
//  PeopleFinder
//
//  Created by Akash Malhotra on 30/11/15.
//  Copyright © 2015 nikhil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"
#import "Operation.h"

@interface PerformDetailOperations : AFHTTPRequestOperation
-(id)initOperationWithSearchString;
-(void)performDetailOperation:(CompletionHandler)successHandler andFailure:(ErrorHandler)errorHandler;
@end
