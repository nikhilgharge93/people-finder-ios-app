
#import "Utilities.h"
#import "AFNetworkReachabilityManager.h"
#import "MBProgressHUD.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Constants.h"

@interface Utilities ()

@end

@implementation Utilities
MBProgressHUD *hud_Progress;

+ (void)setUpUIAppearance {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setBarTintColor:kBrandBlueColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //[[UISearchBar appearance] setBarTintColor:kBrandBlueColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFontBold size:18.0], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:16.0], NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:kFont size:16.0], NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateHighlighted];
    [[UIToolbar appearance] setBarTintColor:kBrandBlueColor];
    [[UIToolbar appearance] setTintColor:[UIColor whiteColor]];
    //    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Gill Sans" size:16.0]} forState:UIControlStateNormal];
    //    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Gill Sans" size:16.0]} forState:UIControlStateSelected];
    //    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    //    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0f/255.0f green:153.0f/255.0f blue:255.0f/255.0f alpha:1.0]} forState:UIControlStateSelected];
    //    [[UISegmentedControl appearance] setTintColor:[UIColor whiteColor]];
    //[[UIPageControl appearance] setPageIndicatorTintColor:[UIColor lightGrayColor]];
    //[[UIPageControl appearance] setCurrentPageIndicatorTintColor:[UIColor kGrayColor]];
}

+ (NSString *)stringInDDMMYYYYFromDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"GMT"]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    return [dateFormatter stringFromDate:date];
}

+ (NSArray *)sortArray:(NSArray *)arrToSort byKey:(NSString *) key ascending:(BOOL) isAsc {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:isAsc];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [arrToSort sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}

+ (BOOL) isConnected {
    //    Commented way of checking Network connectivity using System Configuration Framework.
    //    SCNetworkReachabilityFlags flags;
    //    BOOL receivedFlags;
    //    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [@"google.com" UTF8String]);
    //    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    //    CFRelease(reachability);
    //
    //    if (!receivedFlags || (flags == 0) )
    //    {
    //        return FALSE;
    //    } else {
    //        return TRUE;
    //    }
    
    Reachability * reachability;
    if (!reachability) {
        reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
    }
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        //DLog(@"No Network Reachability!");
        return NO;
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        //DLog(@"WiFi Network Reachability");
        return YES;
    }
    else if (remoteHostStatus == ReachableViaWWAN) {
        // DLog(@"Cellular Network Reachability");
        return YES;
    }
    return NO;
}

+ (void) showHUDWithTextOnly:(NSString *) textToShow onView:(UIView *) view {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = textToShow;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3];
}

+ (void) showBusyIndicator:(NSString *) strMessage withTitle:(NSString *)strTitle toViewController: (UIViewController *)callingVC dimBackground:(BOOL)shouldDimBackground {
    //    NetworkActivityIndicatorVisible(TRUE);
    hud_Progress = [MBProgressHUD showHUDAddedTo:callingVC.view animated:YES];
    hud_Progress.mode = MBProgressHUDModeIndeterminate;
    hud_Progress.labelText = [NSString stringWithFormat:@"%@", strMessage];
    hud_Progress.dimBackground = shouldDimBackground;
}

+ (void) hideHUD {
    [hud_Progress hide:YES];
}

+ (BOOL) isNilString:(NSString *)string {
    BOOL returnVal = YES;
    if(nil != string && 0 < [[Utilities trimString:string] length])
        returnVal = NO;
    return returnVal;
}

+ (NSString *) trimString: (NSString *) string {
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (void)showAlertWithMessage:(NSString *)alertMessage
                  withTitle:(NSString *)alertTitle
         fromViewController:(UIViewController *)callingVC {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:alertTitle
                                                       message:alertMessage
                                                      delegate:callingVC
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [alertView show];
}

+ (BOOL)isEmpty:(id)object {
    return object == nil
    || ([object respondsToSelector:@selector(length)]
        && [(NSData *) object length] == 0)
    || ([object respondsToSelector:@selector(count)]
        && [(NSArray *) object count] == 0) || object == [NSNull null];
}

+ (id)checkForEmpty:(id)object {
    if([self isEmpty:object]) {
        return kEmptyString;
    }
    else
        return object;
}
@end
