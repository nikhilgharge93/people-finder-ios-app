
#ifndef PeopleFinder_Constants_h
#define PeopleFinder_Constants_h

#define isPad() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#   define ELog(err) {if(err) NSLog(@"%@", err);}
#else
#   define DLog(...)
#   define ELog(err)
#endif

#define kApiUrl @"http://www.mocky.io/v2"

#define kUserName           @"UserName"
#define kPassword           @"Password"
#define kPasscode           @"Passcode"
#define kStartedBefore      @"StartedBeforeKey"
#define kbackSpaceChar       @"\u200B"
#define kToHandle              @"TO HANDLE"
#define kAlreadyHandled        @"ALREADY HANDLED"
#define kGrayColor                  [UIColor colorWithRed:224.0/255.0 green:225/255.0 blue:228.0/255.0 alpha:1.0];
#define kCellSelectionBlueColor   [UIColor colorWithRed:23.0f/255.0f green:129.0f/255.0f blue:237.0f/255.0f alpha:1.0]
#define kBrandBlueColor   [UIColor colorWithRed:51.0f/255.0f green:130.0f/255.0f blue:202.0f/255.0f alpha:1.0]
#define kFontBold       @"Arial-BoldMT"
#define kFont            @"Arial"
#define kEmptyString                  @""
#define kKeyboardHeightPortraitMode                               216
#define kKeyboardHeightLandscapeMode                              162
#define kDefaultAnimationDuration                                  0.5f
#define kMinimumScrollFraction                                     0.2f
#define kMaximumScrollFraction                                     0.8f
#define kNewLineCharacter                                          @"\n"
#define kAdminSearch  @"Admin Search"
#endif
