

#import "Reachability.h"


@interface Utilities : NSObject

+ (void)setUpUIAppearance;
+ (NSArray *)sortArray:(NSArray *)arrToSort byKey:(NSString *) key ascending:(BOOL) isAsc;
+ (void) showHUDWithTextOnly:(NSString *) textToShow onView:(UIView *) view;
+ (void) showBusyIndicator:(NSString *) strMessage withTitle:(NSString *)strTitle toViewController: (UIViewController *)callingVC dimBackground:(BOOL)shouldDimBackground ;
+ (void) hideHUD;
+ (BOOL) isNilString:(NSString *) string;
+ (void)showAlertWithMessage:(NSString *)alertMessage
                   withTitle:(NSString *)alertTitle
          fromViewController:(UIViewController *)callingVC;
+ (NSString *)stringInDDMMYYYYFromDate:(NSDate *)date;
+ (BOOL) isConnected;
+ (BOOL)isEmpty:(id)object;
+ (id)checkForEmpty:(id)object;
@end
